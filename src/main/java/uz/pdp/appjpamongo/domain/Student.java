package uz.pdp.appjpamongo.domain;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document
@Getter
@Setter
public class Student {

    @MongoId
    private ObjectId id;

    private String firstName;

    private String lastName;
}
