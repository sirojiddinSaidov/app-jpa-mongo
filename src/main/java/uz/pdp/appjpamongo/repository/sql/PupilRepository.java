package uz.pdp.appjpamongo.repository.sql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appjpamongo.domain.Pupil;

@RepositoryRestResource(path = "pupil")
public interface PupilRepository extends JpaRepository<Pupil, Integer> {
}