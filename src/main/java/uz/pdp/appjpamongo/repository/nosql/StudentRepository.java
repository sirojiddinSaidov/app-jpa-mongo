package uz.pdp.appjpamongo.repository.nosql;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appjpamongo.domain.Student;

@RepositoryRestResource(path = "student")
public interface StudentRepository extends MongoRepository<Student, String> {
}
