package uz.pdp.appjpamongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "uz.pdp.appjpamongo.repository.sql")
@EnableMongoRepositories(basePackages = "uz.pdp.appjpamongo.repository.nosql")
public class AppJpaMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppJpaMongoApplication.class, args);
    }

}
